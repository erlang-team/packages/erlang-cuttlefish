erlang-cuttlefish (3.4.0-1) unstable; urgency=medium

  * New upstream version 3.4.0
  * Update years in debian/copyright
  * Update Standards-Version: 4.7.0 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Thu, 03 Oct 2024 21:26:37 +0200

erlang-cuttlefish (3.2.0-2) unstable; urgency=medium

  * Update debian/rules

 -- Philipp Huebner <debalance@debian.org>  Tue, 26 Dec 2023 14:54:48 +0100

erlang-cuttlefish (3.2.0-1) unstable; urgency=medium

  * New upstream version 3.2.0
  * Update Standards-Version: 4.6.2 (no changes needed)
  * Update years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Sun, 16 Jul 2023 17:18:47 +0200

erlang-cuttlefish (3.1.0-1) unstable; urgency=medium

  * New upstream version 3.1.0
  * Update Standards-Version: 4.6.1 (no changes needed)
  * Update Erlang dependencies
  * Update lintian overiddes

 -- Philipp Huebner <debalance@debian.org>  Wed, 02 Nov 2022 14:19:37 +0100

erlang-cuttlefish (3.0.1-1) unstable; urgency=medium

  * Switch upstream source to fork at https://github.com/Kyorai/cuttlefish
  * New upstream version 3.0.1 (Closes: #935279)
  * Update (Build-)Depends
  * Update Maintainers and Uploaders
  * Update Standards-Version
  * Add lintian overrides
  * Switch debhelper compat level from 11 to 13
  * Change architecture from 'any' to 'all'
  * Update debian/copyright
  * Clean debian/rules
  * Update debian/watch
  * Add lintian overrides
  * Add debian/upstream/metadata

 -- Philipp Huebner <debalance@debian.org>  Sun, 09 Jan 2022 18:44:32 +0100

erlang-cuttlefish (2.0.11+dfsg-4) unstable; urgency=medium

  * Fix FTBFS with Erlang 21.1 (Closes: #909978)
    Update d/patches/remove-warnings_as_errors, and add
    d/patches/support_otp_21.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Mon, 10 Dec 2018 09:07:52 +0900

erlang-cuttlefish (2.0.11+dfsg-3) unstable; urgency=medium

  * Change maintainer address. (Closes: #899489)
  * Update debhelper to 11.
  * Dump Standards-Version to 4.1.4.
  * Change Vcs-Browser and Vcs-Git to salsa.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Mon, 28 May 2018 12:30:46 +0900

erlang-cuttlefish (2.0.11+dfsg-2) unstable; urgency=medium

  * Fix build with OTP 20. (Closes: #871350)
    - Update patches/support_otp_19.
    - Add patches/remove-warnings_as_errors.
  * Bump Standards-Version to 4.0.0.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Sat, 19 Aug 2017 03:11:02 +0900

erlang-cuttlefish (2.0.11+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Update depend version of erlang-neotoma.
  * Bump Standards-Version to 4.0.0.
  * Update debian/copyright.
    Add Files-Excluded: rebar.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Sun, 02 Jul 2017 05:16:16 +0900

erlang-cuttlefish (2.0.10+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Update depend version of erlang-lager.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Wed, 19 Oct 2016 13:09:03 +0900

erlang-cuttlefish (2.0.7+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.8.
  * Update patches/remove_dep.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Sat, 03 Sep 2016 06:37:55 +0900

erlang-cuttlefish (2.0.6+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.7.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Sun, 27 Mar 2016 05:13:26 +0900

erlang-cuttlefish (2.0.5+dfsg-1) unstable; urgency=medium

  * Initial release. (Closes: #815505)

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Mon, 22 Feb 2016 09:22:11 +0900
